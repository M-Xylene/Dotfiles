function cd_using_fzf
    set directory $(fd -t d -d 3 -c always | fzf --ansi --border-label='[Change Directory]')
    if [ -z "$directory" ]
        return 1
    else
        cd $directory
    end
end
