function vi_using_fzf
    set file $(fd -t f -d 3 -c always | fzf --border-label='[Open with Vim]' --preview='bat --color=always {}' --ansi )
    if [ -z "$file" ]
        return 1
    else
        vi $file
    end
end
