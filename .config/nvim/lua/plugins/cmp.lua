--Code completion
local M = {
	"hrsh7th/nvim-cmp",
	event = "InsertEnter",
	dependencies = {
		"hrsh7th/cmp-buffer",
		"hrsh7th/cmp-nvim-lsp",
		"saadparwaiz1/cmp_luasnip",
		{
			"L3MON4D3/LuaSnip",
			event = "InsertEnter",
			dependencies = "rafamadriz/friendly-snippets",
		},
		{
			"windwp/nvim-autopairs",
			lazy = true,
			event = "InsertEnter",
			opts = {},
		},
	},
}

function M.config()
	require("luasnip.loaders.from_vscode").lazy_load()

	local cmp = require("cmp")
	local cmp_select = { behavior = cmp.SelectBehavior.Select }
	local cmp_autopair = require("nvim-autopairs.completion.cmp")

	cmp.event:on("confirm_done", cmp_autopair.on_confirm_done())

	cmp.setup({
		icons = true,
		sources = {
			{ name = "nvim_lsp" },
			{ name = "buffer" },
			{ name = "luasnip" },
		},

		snippet = {
			expand = function(args)
				require("luasnip").lsp_expand(args.body)
			end,
		},

		window = {
			completion = cmp.config.window.bordered(),
			documentation = cmp.config.window.bordered(),
		},

		mapping = cmp.mapping.preset.insert({
			["<CR>"] = cmp.mapping.confirm({ behavior = cmp.ConfirmBehavior.Insert, select = true }),
			["<Tab>"] = cmp.mapping.select_next_item(cmp_select),
		}),
	})
end

-- Icons
M.diagnostics = {
	Error = " ",
	Warn = " ",
	Hint = " ",
	Info = " ",
}

for name, icon in pairs(M.diagnostics) do
	name = "DiagnosticSign" .. name
	vim.fn.sign_define(name, { text = icon, texthl = name, numhl = "" })
end

return M
