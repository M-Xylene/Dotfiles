local M = {
	"folke/trouble.nvim",
	opts = {
		auto_close = true,
		focus = true,
	},
	cmd = "Trouble",
	keys = { { "<leader>tt", "<cmd>Trouble diagnostics toggle filter.buf=0<cr>" } },
}

return M
