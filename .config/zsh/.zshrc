autoload -U colors && colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
setopt autocd

export HISTCONTROL=ignoredups

HISTSIZE=100000
SAVEHIST=100000
HISTFILE="${XDG_CACHE_HOME:-$HOME/.cache}/zsh/history"

autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
# Include hidden files.
_comp_options+=(globdots)

bindkey -v
export KEYTIMEOUT=1

bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

bindkey ^R history-incremental-search-backward
bindkey '^j' vi-cmd-mode
bindkey '^g' genqr

source $HOME/.config/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh 2> /dev/null
source $HOME/.config/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.plugin.zsh 2> /dev/null
[[ -r "/usr/share/z/z.sh" ]] && source /usr/share/z/z.sh


        ##################################################### ALIASES #############################################################

alias qt='cd ~/.config/qtile/'
alias genqr='curl qrenco.de/https://'
alias xm='cd ~/.config/xmonad/'
alias i3='cd ~/.config/i3/'
alias dm='cd ~/.config/suckless/dwm/'
alias zso='source $HOME/.config/zsh/.zshrc'
alias dmenu_run='dmenu_run -c -p " " -l 10'
alias genqr='curl qrenco.de/https://'
alias svi='sudoedit'
alias sl='cd ~/.config/suckless/slstatus/'
# alias so='source $HOME/.config/fish/config.fish'
alias ls='exa -l -g --icons'
alias ll='ls --tree --level=2 -a'

alias glab='/usr/bin/git --git-dir=$HOME/Stuff/Dotfiles --work-tree=$HOME'
alias gpsh='glab push origin master'
alias gcmt='glab commit'
alias gad='glab add'

# alias pkgs='pacman -Qq | wc -l'
# alias plst='echo -n "Packages installed:- " ; pkgs'
# alias plst='pacman -Qq | fzf --preview "pacman -Qil {}" --bind "enter:execute(pacman -Qil {} | less)"'
alias batstatus='upower -i /org/freedesktop/UPower/devices/battery_BAT0'
# alias gcln='git clone'
alias mkdir='mkdir -p'
alias sync='sudo pacman -Sy'
 alias tls='tmux ls'
alias update='lastUpdate.sh && sudo pacman -Syu && xmonad --recompile && upDate.sh'
 alias tmx='tmux -u attach || tmux'
alias install='sudo pacman -S'
# alias ftinst='flatpak install'
alias remove='sudo pacman -Rns'
# alias srd='rm -r'
# alias res='gio trash --restore trash:///'
# alias rm='gio trash '
# alias empty='gio trash --empty'
# alias tlst='gio trash --list'
alias pclean='remove $(pacman -Qtdq)'
# alias pinst='paru -S'
alias clean='sudo pacman -Sc'
# alias find='sudo pacman -Ss'
# alias battery='notify-send $("acpi")'
