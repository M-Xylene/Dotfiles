static const unsigned int borderpx = 1;
static const unsigned int snap = 32;
static const unsigned int systraypinning = 0;
static const unsigned int systrayonleft = 0;
static const unsigned int systrayspacing = 3;
static const int systraypinningfailfirst = 1;
static const unsigned int gappx = 5;
static const int showsystray = 1;
static const int showbar = 1;
static const int topbar = 1;
static const char *fonts[] = {"JetBrains Mono Nerd Font:pixelsize=14"};
static const char dmenufont[] = "JetBrains Mono Nerd Font:pixelsize=16";
static const char col_gray1[] = "#222222";
static const char col_gray2[] = "#444444";
static const char col_gray3[] = "#bbbbbb";
static const char col_gray4[] = "#eeeeee";
static const char col_border[] = "#d65d0e";
static const char col_cyan[] = "#005577";
static const char *colors[][3] = {
    [SchemeNorm] = {col_gray3, col_gray1, col_gray2},
    [SchemeSel] = {col_gray4, col_cyan, col_border},
};

static const char *tags[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};

static const Rule rules[] = {
    {"Firefox", NULL, NULL, 1 << 8, 0, -1},
    {"Brave-browser", NULL, NULL, 1 << 1, 0, -1},
    {"qutebrowser", NULL, NULL, 1 << 1, 0, -1},
};

static const float mfact = 0.50;
static const int nmaster = 1;
static const int resizehints = 1;
static const int lockfullscreen = 1;

static const Layout layouts[] = {
    {" ", tile},
    {" ", monocle},
    {"><>", NULL},
};

#define MODKEY Mod1Mask
#define TAGKEYS(KEY, TAG)                                                      \
  {MODKEY, KEY, view, {.ui = 1 << TAG}},                                       \
      {MODKEY | ControlMask, KEY, toggleview, {.ui = 1 << TAG}},               \
      {MODKEY | ShiftMask, KEY, tag, {.ui = 1 << TAG}},                        \
      {MODKEY | ControlMask | ShiftMask, KEY, toggletag, {.ui = 1 << TAG}},

#define SHCMD(cmd)                                                             \
  {                                                                            \
    .v = (const char *[]) { "/bin/sh", "-c", cmd, NULL }                       \
  }

static char dmenumon[2] = "0";
static const char *dmenucmd[] = {
    "dmenu_run", "-m",      dmenumon, "-fn",    dmenufont, "-nb",     col_gray1,
    "-nf",       col_gray3, "-sb",    col_cyan, "-sf",     col_gray4, NULL};
static const char *termcmd[] = {"kitty", NULL};
static const char *tmuxcmd[] = {"kitty", "-e", "tmux", "-u", "-2", NULL};

static const Key keys[] = {
    /* modifier                     key        function        argument */
    {ControlMask, XK_space, spawn, {.v = dmenucmd}},
    {MODKEY, XK_Return, spawn, {.v = tmuxcmd}},
    {MODKEY | ShiftMask, XK_Return, spawn, {.v = termcmd}},
    {MODKEY, XK_e, spawn, SHCMD("kitty -e yazi")},
    {MODKEY | ShiftMask, XK_b, togglebar, {0}},

    /* Volume */
    {ControlMask | ShiftMask, XK_Up, spawn, SHCMD("DunstVol up")},
    {ControlMask | ShiftMask, XK_Down, spawn, SHCMD("DunstVol down")},
    {ControlMask | ShiftMask, XK_m, spawn, SHCMD("DunstVol mute")},

    /* Brightness */
    {ControlMask | ShiftMask, XK_Right, spawn, SHCMD("Brightness up")},
    {ControlMask | ShiftMask, XK_Left, spawn, SHCMD("Brightness down")},

    /* Screenshot */
    {0, XK_Print, spawn,
     SHCMD("scrot '%Y-%m-%d_%H-%M-%S.png' -e 'mv $f ~/Pictures/Screenshots/' "
           "-q 100")},
    {ControlMask, XK_Print, spawn,
     SHCMD("import png:- | xclip -selection clipboard -t image/png")},

    /* Dmenu Scripts */
    {MODKEY | ShiftMask, XK_l, spawn, SHCMD("dm-logout")},
    {MODKEY | ShiftMask, XK_c, spawn, SHCMD("dm-confedit")},

    /* Switching Focus */
    {MODKEY, XK_l, focusstack, {.i = +1}},

    /* Inc/Dec window size */
    {MODKEY | ShiftMask, XK_h, setmfact, {.f = -0.01}},

    /* Killing a window */
    {MODKEY, XK_c, killclient, {0}},

    /* Layouts */
    {MODKEY | ShiftMask, XK_t, setlayout, {.v = &layouts[0]}},
    {MODKEY, XK_m, setlayout, {.v = &layouts[1]}},
    {MODKEY, XK_f, setlayout, {.v = &layouts[2]}},
    {MODKEY | ShiftMask, XK_space, setlayout, {0}},
    {ControlMask | ShiftMask, XK_space, togglefloating, {0}},
    {MODKEY, XK_0, view, {.ui = ~0}},
    {MODKEY | ShiftMask, XK_0, tag, {.ui = ~0}},
    {MODKEY, XK_comma, focusmon, {.i = -1}},
    {MODKEY, XK_period, focusmon, {.i = +1}},
    {MODKEY | ShiftMask, XK_comma, tagmon, {.i = -1}},
    {MODKEY | ShiftMask, XK_period, tagmon, {.i = +1}},

    /* Gaps */
    {MODKEY, XK_minus, setgaps, {.i = -1}},
    {MODKEY, XK_equal, setgaps, {.i = +1}},
    {MODKEY | ShiftMask, XK_equal, setgaps, {.i = 0}},

    TAGKEYS(XK_1, 0) TAGKEYS(XK_2, 1) TAGKEYS(XK_3, 2) TAGKEYS(XK_4, 3)
        TAGKEYS(XK_5, 4) TAGKEYS(XK_6, 5) TAGKEYS(XK_7, 6) TAGKEYS(XK_8, 7)
            TAGKEYS(XK_9, 8){MODKEY | ShiftMask, XK_q, quit, {0}},
    {MODKEY, XK_q, quit, {1}},
};

static const Button buttons[] = {
    /* click                event mask      button          function argument */
    {ClkTagBar, MODKEY, Button1, tag, {0}},
    {ClkTagBar, MODKEY, Button3, toggletag, {0}},
    {ClkStatusText, 0, Button2, spawn, {.v = termcmd}},
    {ClkClientWin, MODKEY, Button1, movemouse, {0}},
    {ClkClientWin, MODKEY, Button2, togglefloating, {0}},
    {ClkClientWin, MODKEY, Button3, resizemouse, {0}},
    {ClkTagBar, 0, Button1, view, {0}},
    {ClkTagBar, 0, Button3, toggleview, {0}},
    {ClkTagBar, MODKEY, Button1, tag, {0}},
    {ClkTagBar, MODKEY, Button3, toggletag, {0}},
};
