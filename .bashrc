# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1="\e[1;31m[\e[0m\e[1;33m\u\e[0m\e[1;32m@\e[0m\e[1;34m\h\e[0m\e[1;36m \w\e[0m\e[1;31m]\e[0m\$ "
#
        ##################################################### ALIASES #############################################################

alias qt='cd ~/.config/qtile/'
alias genqr='curl qrenco.de/https://'
alias xm='cd ~/.xmonad/'
alias i3='cd ~/.config/i3/'
alias dm='cd ~/.config/suckless/dwm/'
alias dmenu_run='dmenu_run -c -p "Apps:" -l 10'
alias svi='sudoedit'
alias sl='cd ~/.config/suckless/slstatus/'
# alias so='source $HOME/.config/fish/config.fish'
# alias ls='ls --color=auto'
alias ls='exa -l -g --icons'
alias ll='ls --tree --level=1 -a'

alias glab='/usr/bin/git --git-dir=$HOME/Stuff/Dotfiles --work-tree=$HOME'
alias gpsh='glab push origin master'
alias gcmt='glab commit'
alias gad='glab add'

# alias pkgs='pacman -Qq | wc -l'
# alias plst='echo -n "Packages installed:- " ; pkgs'
# alias plst='pacman -Qq | fzf --preview "pacman -Qil {}" --bind "enter:execute(pacman -Qil {} | less)"'
alias batstatus='upower -i /org/freedesktop/UPower/devices/battery_BAT0'
# alias gcln='git clone'
alias mkdir='mkdir -p'
alias sync='sudo pacman -Sy'
# alias tls='tmux ls'
alias update='sudo pacman -Syu'
# alias tmx='tmux -u attach || tmux'
alias install='sudo pacman -S'
# alias ftinst='flatpak install'
alias remove='sudo pacman -Rns'
# alias srd='rm -r'
# alias res='gio trash --restore trash:///'
# alias rm='gio trash '
# alias empty='gio trash --empty'
# alias tlst='gio trash --list'
alias pclean='remove $(pacman -Qtdq)'
# alias pinst='paru -S'
alias clean='sudo pacman -Sc'
# alias find='sudo pacman -Ss'
# alias battery='notify-send $("acpi")'
. "$HOME/.cargo/env"
