#+title: Configuration files for almost all necessary applications.

* Requirement
 GNU Stow

* Want to replicate the ecosystem ?
** Using GNU Stow
If you want to use the same configuration, the easiest way to do so is by using [[https://www.gnu.org/software/stow/][GNU Stow]].

**** NOTE:- To properly use GNU Stow clone this repository in the home directory only.
#+begin_src sh
git clone https://gitlab.com/M-Xylene/dotfiles.git ~/.dotfiles
cd ~/.dotfiles
stow .
#+end_src

** Manual way
If you don't like [[https://en.wikipedia.org/wiki/Symbolic_link][symlinks]] in your filesystem, you can manually place these configs at their respective places.
Just clone the repository and copy the contents.

* Looks
** Xmonad window manager
Primarly I use [[https://xmonad.org/][Xmonad WM]] and this is how it looks.
[[file:./.github/Xmonad.png]]
