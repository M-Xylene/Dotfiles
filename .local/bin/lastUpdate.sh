#!/bin/sh

export DISPLAY=:0
export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/1000/bus"

if [ -f $DATEFILE ]; then
    LASTDATE=`cat ~/.lastupdate.txt`
else
    LASTDATE=""
fi

DATEFILE=~/.lastupdate.txt

if [ -f $DATEFILE ]; then
    notify-send "Last Update:" "$LASTDATE" -i "update" -r 9991
elif [ ! -f $DATEFILE ]; then
    notify-send "No data found" -i "alert" -r 9991
fi
